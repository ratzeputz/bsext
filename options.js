/** See License file for more information.
 *
 *  This file handle the extension option handling.
 *  It restores and saves the desired configuration.
 *  It's frontend is options.html
 */
'use strict';

/* set the status element to empty */
function status(msg) {
    document.querySelector('#status').textContent = msg;
    setTimeout(function() {
        document.querySelector('#status').textContent = '';
    }, 1000);
}

/* Saves extension options to google profile and reply to user */
function save_options() {
    chrome.storage.sync.set({
        hover: document.querySelector('#hover').checked,
        previous: document.querySelector('#previous').checked,
        delay: document.querySelector('#delay').value,
    }, status('Options saved'));
}
document.querySelector('#hover').addEventListener('click', save_options);
document.querySelector('#previous').addEventListener('click', save_options);
document.querySelector('#delay').addEventListener('keyup',
                                                  function (e){
                                                      event.preventDefault();
                                                      if (event.keyCode === 13) {
                                                          save_options();
                                                      }});
document.querySelector('#reload').addEventListener('click', function() {
    chrome.runtime.getBackgroundPage(function(backgroundPage){
        backgroundPage.init_local_db();
    });
});

/* restore options from the foofle profile at dom startup */
function restore_options() {
    chrome.storage.sync.get(
        ['previous', 'hover', 'delay'],
        function(items) {
            document.querySelector('#hover').checked = items.hover;
            document.querySelector('#previous').checked = items.previous;
            document.querySelector('#delay').value = items.delay;
        });
}
restore_options();
