/** See License file for more information.
 *
 * This script if enabled provides covers of the series
 * if the mouse hovers the element link. The cover
 * appears in the upper right side for convenience.
 */
'use strict';

const add_fn = (id, e, time) => {
    let timer;
    e.addEventListener('mouseover', function() {
        timer = setTimeout(function () {
            e.classList.add('cover');
        if (!e.querySelector('img')) {
            let url = '/public/images/cover/' + id + '.jpg';
            e.innerHTML += '<img class="cover-content" src="' + url + '">';
        }
            e.querySelector('img').style.opacity = 1;
        }, time * 1000);
    }, function() {
        timer = setTimeout(function () {
            e.querySelector('img').style.opacity = 0;
        });
    });
    e.addEventListener('mouseout', function () {
        clearTimeout(timer);
    });
};

/* add cover hover to every series link */
const hover = (time) => {
    let series = document.querySelectorAll('#seriesContainer ul li a');
    chrome.storage.local.get(['ids'], function(items) {
        series.forEach(function(e) {
            if (items.ids[e.textContent] != undefined) {
                add_fn(items.ids[e.textContent].id, e, time);
            }
        });
    });
};

/* if the user has enabled this feature show series covers */
const enable_hover = (h) => {
    if (h.hover) {
        hover(h.delay);
    }
};
chrome.storage.sync.get(['hover', 'delay'], enable_hover);
