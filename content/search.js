/** See License file for more information.
 *
 * Search in the favorite lists on all sites
 */

document.querySelector('#other-series-nav > ul').innerHTML
    = '<li id="fav_search"><input type="text" id="search_favs" style="padding: 3px 3px"/></i>'
    + document.querySelector('#other-series-nav > ul').innerHTML;
document.querySelector('#search_favs').setAttribute('placeholder', 'Favoriten durchsuchen');
document.querySelector('#search_favs').setAttribute('autocomplete', 'off');

/* search on keypress an display mathing entries */
let timeout = null;
const search = (e) => {
    clearTimeout(timeout); /* kill timeout if another event is triggered */
    let input = document.querySelector('#search_favs').value.toLowerCase();
    let regex = new RegExp(input, 'i');
    timeout = setTimeout(function() {

        document.querySelectorAll('#other-series-nav > ul li')
            .forEach(function(e) {
                if (e.id != 'fav_search') {
                    let fav = e.textContent.toLowerCase();
                    let rep = e.textContent.replace(regex, '<strong>$&</strong>');
                    e.style.display = fav.indexOf(input) !== -1 ? 'block' : 'none';
                    e.firstElementChild.innerHTML = rep;
                }
            });
    }, 300); /* set timeout to 300 msecs */
};
document.querySelector('#search_favs').addEventListener('keyup',search);

/* take the first visible match on enter */
const jump_to_series = (e) => {
    if (e.which == 13) { /* take the first visible serie */
        let fav = document.querySelectorAll('#other-series-nav ul li:visible')[1];
        window.location.href = $(fav).children().get(0).href;
    }
};
document.querySelector('#search_favs').addEventListener('keypress', jump_to_series);
