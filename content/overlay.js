/** See License file for more information.
 *
 *  This script introduces an overlay to the andere-serien site.
 *  It indicates wether a serie was partly or fully watched.
 *  The nobreak wrapper removes the need for mutation watching
 *  since the search function replaces content of link elements
 *  and images appended to the li child creates linebreaks.
 */
'use strict';

/* actual toggle a series panes */
const toggle = (i) => {
    chrome.storage.local.get(['toggle'], function(items) {
        if (items.toggle == null) {
            items.toggle = {}; /* we need a object for the toggle states */
        }

        let e = document.querySelector('#toggle' + i);
        if (e.style.display == "block" || !e.style.display) {
            items.toggle['toggle' + i] = true;
            e.style.display = "none";
        } else {
            items.toggle['toggle' + i] = false;
            e.style.display = "block";
        }
        chrome.storage.local.set(items);
    });
};

/* make genre containers toggleable and remember last setting */
chrome.storage.local.get(['toggle'], function(items) {
    document.querySelectorAll('.genre').forEach(function(e, i) {
        e.children[1].id = "toggle" + i;
        e.querySelector('span').addEventListener('click', function() {toggle(i); return false;});

        if (items.toggle['toggle' + i] === true) {
            document.querySelector('#toggle' + i).style.display = "none";
        }
    });
});

// add jump to first series on enter
document.querySelector('#serForm').addEventListener('keypress', function(event) {
    if (event.which == 13) {
        let e = document.querySelector('#seriesContainer li');
        window.location.href = e.firstElementChild.href;
    }
});
