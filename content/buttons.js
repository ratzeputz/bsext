/** See License file for more information.
 *
 * This small content script implements a next and previous button.
 *  it chooses the next episode of the current season or
 *  the next season if the last episode of a season is reached.
 */

'use strict';
/* do an async request to fetch the season informations */
async function fetchSeason(href) {
    let response = await fetch(href);
    return await response.text();
}

const get_links = (data) => {
    let html = document.createElement('html');
    html.innerHTML = data;
    return html.querySelectorAll('.episodes a');
};

/* adds a next btn to the upper right side of the hoster choice bar
 * if no hoster is found, the language tag gets append which has no influence */
const add_btn = (href, name) => {
    let hoster = location.href.substring(location.href.lastIndexOf('/'));
    document.querySelector('ul.hoster-tabs.top').innerHTML += '<li><a href="' + href + hoster +  '">' + name + '</a></li>';
};

/* fetch the previous or next season and provide a btn */
const season = (direction, name) => {
    let nseason = document.querySelector('#seasons > ul > li[class*=\'active\']')[direction];
    if (nseason) { /* found a next season */
        fetchSeason(nseason.firstElementChild.href)
            .then(data => {
                let links = get_links(data);
                if (direction.startsWith('next')) {
                    add_btn(links[0].href, name);
                } else {
                    let href = links[links.length -1].href.substring(0, links[links.length -1].href.lastIndexOf('/'));
                    add_btn(href, name);
                }
            }).catch(err => console.log(err));
    }
};

/* provide a btn to the next or previous episode across seasons and languages boundaries */
const select_sibling = (direction, name) => {
    let elem = document.querySelector('#episodes li[class*=\'active\']')[direction];
    while (elem && elem.className.trim().endsWith("disabled")) {
        elem = elem.nextElementSibling;
    }

    if (!elem) { /* check if we're at the end of the season */
        season(direction, name);
    } else { /* just a normal link */
        add_btn(elem.firstElementChild.href, name);
    }
};

select_sibling("nextElementSibling", ">");
/* add a previous btn if requested */
const prev_btn = (enabled) => {
    console.log(enabled);
    if (enabled.previous) {
        select_sibling("previousElementSibling", "<");
    }
};
chrome.storage.sync.get(['previous'], prev_btn);
