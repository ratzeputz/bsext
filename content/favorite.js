/** See License file for more information.
 *
 * This content script extends the series sites to let the user
 *  mark favorites more easaly.
 */
'use strict';

/* update the favorite by posting against the bs page. */
const update_favs = async (favs) => {
    let list = [];
    for (const key in favs) {
        list.push(favs[key]);
    }
    let token = document.querySelector('meta[name=security_token]').attributes.content.value;
    fetch("https://bs.to/ajax/edit-seriesnav.php", {
        method: 'POST',
        refer: 'https://bs.to/settings/series',
        body: 'token=' + token + '&' + 'series%5B%5D=' + list.join('&series%5B%5D='),
        headers: {
            'Content-Type': "application/x-www-form-urlencoded",
        },
    }).then(data => {
        chrome.storage.local.set({favs});
        window.location.href = window.location.href;
    }).catch(err => console.log(err));
};

/* set update callback */
const set_link = (text, favs) => {
    document.querySelector('#sp_right a:last-child').innerHTML += text;
    document.querySelector('#sp_right a:last-child')
        .addEventListener('click', function() {update_favs(favs); return false;});
};

/* add the favorite link to the series site */
chrome.storage.local.get(['ids', 'favs'], function(items) {
    let serie = document.querySelector('#sp_left > h2').firstChild.textContent.trim();
    /* define favorite link */
    document.querySelector('#sp_right').innerHTML
        += '<a href="javascript:void(0)"><span class="fas fa-star fa-align-right"></span></a>';

    /* get the id for the current series if existant */
    if (items.ids != null || items.favs != null) {
        let id = items.ids[serie].id;
        if (items.favs[serie] != null) {
            delete items.favs[serie];
            set_link('Aus den Favoriten entfernen', items.favs);

        } else if (id != null){ /* offer add favorite */
            items.favs[serie] = id;
            set_link('Zu den Favoriten hinzufügen', items.favs);

        } else { /* no series id */
            console.log("Update the internal database by reloading the plugin.");
            set_link('ID nicht gefunden', items.favs);
        }
    }
});
