/** See License file for more information.
 *
 * This background script only sets the default configuration
 * and activates the extension icon if the tab is on the bs site.
 * It initialises the database with series name to id mapping.
 */

'use strict';

chrome.runtime.onInstalled.addListener(function() {
    /* set default configuration */
    let options = {hover: true, previous: false, delay: 1.5};
    chrome.storage.sync.set(options);
    init_local_db();

    /* activate extension icon on bs site */
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [new chrome.declarativeContent.PageStateMatcher({
                pageUrl: {hostEquals: 'bs.to'},
            })],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });
});

/* do an async request to fetch the season informations */
async function fetchSeries(href) {
    let response = await fetch(href);
    return await response.text();
}

/* fetch the id's of all series currently defined under /settings/series */
function init_local_db() {
    fetchSeries("https://bs.to/settings/series")
        .then(data => {
            let html = document.createElement('html');
            html.innerHTML = data;

            let favs = {};
            html.querySelectorAll('#series-menu > li').forEach(function(e) {
                favs[e.textContent] = e.dataset.id;
            });

            let ids = {};
            html.querySelectorAll('#waste1 > li, #waste2 > li, #series-menu > li')
                .forEach(function(e) {
                    ids[e.textContent] = {id: e.dataset.id};
                });
            chrome.storage.local.set({ids, favs});
        }).catch(err => console.log(err));
}
