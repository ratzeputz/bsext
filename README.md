
## bsext

A small chrome extension for bs which adds some modern functionality.

Installation:

* Either clone the repository or use the 
  [archive](https://gitlab.com/ratzeputz/bsext/-/archive/master/bsext-master.zip)
* Open the page `chrome://extensions` within Chrome
* Activate the developer mode and load the unpacked extension

For most of the features a user account at bs.to is required.

#### Features

* Provide a next and previous button on the hoster page
* Provide a favorite link on the series page.
* Search through favorites.
* Cover Hover: Shows the serie cover on the upper right side on mouse hover on a link.
  - Images are loaded directly on the hover event
* Toggle visbility of genre groups.


#### Wishlist

  * Open for suggestions
  * use bg.js for functionality and use message passing? (partly)
  * autoplay (hard, due to missing content within the iframe)

#### problems

One major problem is that if a new serie are added the extension
doesn't know of this update.
One solution is to regulary update the internal database either
manualy or with an updater (no updater implmented at the moment).

At the moment the db needs to be build manually within the options
page and should be cleared before rebuilding.

